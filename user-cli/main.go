package main

import (
	"context"
	"log"
	"os"

	pb "bitbucket.org/losaped/shippy/user-service/proto/user"
	micro "github.com/micro/go-micro"
)

func main() {
	svc := micro.NewService(micro.Name("user.client"))
	svc.Init()

	client := pb.NewUserService("user", svc.Client())

	newUser := pb.User{
		Name:     "Roman Poletaev",
		Email:    "losaped@gmail.com",
		Password: "mysecretpass",
		Company:  "apla",
	}

	log.Println("trying to create new user:", newUser)
	// Call our user service
	r, err := client.Create(context.TODO(), &newUser)

	if err != nil {
		log.Fatalf("Could not create: %v", err)
	}

	log.Printf("Created: %s", r.User.Id)

	getAll, err := client.GetAll(context.Background(), &pb.Request{})
	if err != nil {
		log.Fatalf("Could not list users: %v", err)
	}

	log.Println("Users received")
	for _, v := range getAll.Users {
		log.Println(v)
	}

	authResponse, err := client.Auth(context.Background(), &pb.User{
		Email:    newUser.Email,
		Password: newUser.Password,
	})

	if err != nil {
		log.Fatalf("Could not authenticate user: %s error: %v\n", newUser.Email, err)
	}

	log.Printf("Your access token is: %s \n", authResponse.Token)
	// eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJVc2VyIjp7ImlkIjoiMTE3NDNmMDMtYjU3Yi00ODFlLTlhNmEtNzMyOGI3MTg4NTNjIiwibmFtZSI6IlJvbWFuIFBvbGV0YWV2IiwiY29tcGFueSI6ImFwbGEiLCJlbWFpbCI6Imxvc2FwZWRAZ21haWwuY29tIiwicGFzc3dvcmQiOiIkMmEkMTAkcWZ5UDVPcG4zdjFidkRWMWtHZ0pvZUN1dzF2aW9nQ1VHb3JoY09KTlNJVjBsaE1CVGVCNVMifSwiZXhwIjoxNTU1MDE4OTEyLCJpc3MiOiJnby5taWNyby5zcnYudXNlciJ9.vVW2hUaFo3QGZAUzH_2oJX45AXb9PWdrGOcMdar7uPc

	os.Exit(0)
	// if err := svc.Run(); err != nil {
	// 	log.Println(err)
	// }
}
