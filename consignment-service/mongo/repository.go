package mongo

import (
	pb "bitbucket.org/losaped/shippy/consignment-service/proto/consignment"
	mgo "gopkg.in/mgo.v2"
)

const consignmentCollection = "consignments"

type ConsignmentRepository struct {
	session *mgo.Session
	host    string
	dbName  string
}

func CreateConsignmentRepository(host, dbName string) (*ConsignmentRepository, error) {
	session, err := mgo.Dial(host)
	if err != nil {
		return nil, err
	}

	session.SetMode(mgo.Monotonic, true)

	return &ConsignmentRepository{
		session: session,
		host:    host,
		dbName:  dbName,
	}, nil
}

func (repo *ConsignmentRepository) Close() {
	repo.session.Close()
}

func (repo *ConsignmentRepository) Create(consignmennt *pb.Consignment) error {
	s := repo.session.Clone()
	defer s.Close()

	c := s.DB(repo.dbName).C(consignmentCollection)
	return c.Insert(consignmennt)
}

func (repo *ConsignmentRepository) GetAll() (consignments []*pb.Consignment, err error) {
	s := repo.session.Clone()
	defer s.Close()

	c := s.DB(repo.dbName).C(consignmentCollection)
	err = c.Find(nil).All(&consignments)
	return
}
