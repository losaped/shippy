package mongo

import (
	pb "bitbucket.org/losaped/shippy/vessel-service/proto/vessel"
	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

const vesselCollection = "vessels"

type VesselRepository struct {
	session *mgo.Session
	host    string
	dbName  string
}

func CreateVesselRepository(host, dbName string) (*VesselRepository, error) {
	session, err := mgo.Dial(host)
	if err != nil {
		return nil, err
	}

	session.SetMode(mgo.Monotonic, true)

	return &VesselRepository{
		session: session,
		host:    host,
		dbName:  dbName,
	}, nil
}

func (repo *VesselRepository) Close() {
	repo.session.Close()
}

func (repo *VesselRepository) Create(vessel *pb.Vessel) error {
	s := repo.session.Clone()
	defer s.Close()

	c := s.DB(repo.dbName).C(vesselCollection)
	return c.Insert(vessel)
}

func (repo *VesselRepository) FindAvailable(spec *pb.Specification) (vessel *pb.Vessel, err error) {
	s := repo.session.Clone()
	defer s.Close()

	c := s.DB(repo.dbName).C(vesselCollection)

	err = c.Find(bson.M{
		"capacity":  bson.M{"$gte": spec.Capacity},
		"maxweight": bson.M{"$gte": spec.MaxWeight},
	}).One(&vessel)
	return
}
