package main

import (
	"fmt"
	"log"
	"os"

	mng "bitbucket.org/losaped/shippy/vessel-service/mongo"
	pb "bitbucket.org/losaped/shippy/vessel-service/proto/vessel"
	micro "github.com/micro/go-micro"
)

type Repository interface {
	FindAvailable(*pb.Specification) (*pb.Vessel, error)
	Create(*pb.Vessel) error
	Close()
}

type service struct {
	repo Repository
}

func createDummyData(repo Repository) {
	vessels := []*pb.Vessel{
		{Id: "vessel001", Name: "Kane's Salty Secret", MaxWeight: 200000, Capacity: 500},
	}
	for _, v := range vessels {
		repo.Create(v)
	}
}

func main() {

	dbHost := os.Getenv("DB_HOST")
	dbName := os.Getenv("DB_NAME")
	repo, err := mng.CreateVesselRepository(dbHost, dbName)
	if err != nil {
		log.Fatal(err)
	}

	log.Println("mongo connection established")
	defer repo.Close()

	createDummyData(repo)

	h := handler{repo: repo}
	srv := micro.NewService(micro.Name("vessel"))

	srv.Init()

	pb.RegisterVesselServiceHandler(srv.Server(), &h)

	if err := srv.Run(); err != nil {
		fmt.Println(err)
	}
}
