package main

import (
	"context"
	"log"

	pb "bitbucket.org/losaped/shippy/consignment-service/proto/consignment"
	vesselpb "bitbucket.org/losaped/shippy/vessel-service/proto/vessel"
)

type handler struct {
	repo      Repository
	vesselCli vesselpb.VesselService
}

func (h *handler) CreateConsignment(ctx context.Context, req *pb.Consignment, res *pb.Response) error {
	spec := &vesselpb.Specification{
		MaxWeight: req.Weight,
		Capacity:  int32(len(req.Containers)),
	}

	vesselsResp, err := h.vesselCli.FindAvailable(context.Background(), spec)

	log.Printf("%+v\n", spec)
	if err != nil {
		log.Println("ERROR:", err)
		return err
	}

	log.Printf("Found vessel: %s \n", vesselsResp.Vessel.Name)
	req.VesselId = vesselsResp.Vessel.Id

	if err := h.repo.Create(req); err != nil {
		return err
	}

	res.Created = true
	res.Consignment = req
	return nil
}

func (h *handler) GetConsignments(ctx context.Context, req *pb.GetRequest, res *pb.Response) error {
	cons, err := h.repo.GetAll()
	res.Consignments = cons
	return err
}
