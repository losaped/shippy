package main

import (
	"context"
	"errors"
	"fmt"

	"golang.org/x/crypto/bcrypt"

	pb "bitbucket.org/losaped/shippy/user-service/proto/user"
)

type Handler struct {
	repo         Repository
	tokenService Authable
}

func (srv *Handler) GetAll(ctx context.Context, req *pb.Request, res *pb.Response) error {
	users, err := srv.repo.GetAll()
	if err != nil {
		return nil
	}

	res.Users = users
	return nil
}

func (srv *Handler) Get(ctx context.Context, req *pb.User, res *pb.Response) error {
	user, err := srv.repo.Get(req.Id)
	if err != nil {
		return err
	}

	res.User = user
	return nil
}

func (srv *Handler) Auth(ctx context.Context, req *pb.User, res *pb.Token) error {
	user, err := srv.repo.GetByEmail(req.Email)
	if err != nil {
		return err
	}

	if user == nil {
		return fmt.Errorf("user with email: %s not found", req.Email)
	}

	if err := bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(req.Password)); err != nil {
		return err
	}

	token, err := srv.tokenService.Encode(user)
	if err != nil {
		return err
	}

	res.Token = token
	return nil
}

func (srv *Handler) Create(ctx context.Context, req *pb.User, res *pb.Response) error {
	hashedPass, err := bcrypt.GenerateFromPassword([]byte(req.Password), bcrypt.DefaultCost)
	if err != nil {
		return err
	}

	req.Password = string(hashedPass)
	if err := srv.repo.Create(req); err != nil {
		return err
	}

	res.User = req
	return nil
}

func (srv *Handler) ValidateToken(ctx context.Context, req *pb.Token, res *pb.Token) error {
	claims, err := srv.tokenService.Decode(req.Token)

	if err != nil {
		fmt.Println("error on decode token", req.Token)
		return err
	}

	if claims.User.Id == "" {
		return errors.New("invalid user")
	}

	res.Valid = true
	return nil
}
