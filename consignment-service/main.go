package main

import (
	"context"
	"fmt"
	"log"
	"os"

	mng "bitbucket.org/losaped/shippy/consignment-service/mongo"

	pb "bitbucket.org/losaped/shippy/consignment-service/proto/consignment"
	userpb "bitbucket.org/losaped/shippy/user-service/proto/user"
	vesselpb "bitbucket.org/losaped/shippy/vessel-service/proto/vessel"
	micro "github.com/micro/go-micro"
	"github.com/micro/go-micro/metadata"
	"github.com/micro/go-micro/server"
)

const (
	port = ":50051"
)

type Repository interface {
	Create(*pb.Consignment) error
	GetAll() ([]*pb.Consignment, error)
	Close()
}

var consignmentsService micro.Service

func main() {

	dbHost := os.Getenv("DB_HOST")
	dbName := os.Getenv("DB_NAME")

	repo, err := mng.CreateConsignmentRepository(dbHost, dbName)
	if err != nil {
		log.Fatal(err)
	}

	log.Println("mongo connection established")
	defer repo.Close()

	consignmentsService = micro.NewService(
		micro.Name("consignment"),
		micro.WrapHandler(AuthWrapper),
	)
	consignmentsService.Init()

	vesselClient := vesselpb.NewVesselService("vessel", consignmentsService.Client())
	handler := &handler{
		repo:      repo,
		vesselCli: vesselClient,
	}

	pb.RegisterShippingServiceHandler(consignmentsService.Server(), handler)

	if err := consignmentsService.Run(); err != nil {
		log.Fatalf("failed to serve: %v", err)
	}
}

func AuthWrapper(fn server.HandlerFunc) server.HandlerFunc {
	return func(ctx context.Context, req server.Request, rsp interface{}) error {
		meta, ok := metadata.FromContext(ctx)
		if !ok {
			return fmt.Errorf("no auth meta-data found in request")
		}

		token := meta["Token"]
		log.Println("Authenticating with token: ", token)

		usc := userpb.NewUserService("user", consignmentsService.Client())
		_, err := usc.ValidateToken(ctx, &userpb.Token{
			Token: token,
		})

		if err != nil {
			fmt.Println("error on validate token: ", token)
			return err
		}

		// r := client.NewRequest("user", "UserService.ValidateToken", userpb.Token{
		// 	Token: token,
		// })

		//  := &userpb.Token{}
		// if err := client.Call(context.Background(), r, userRsp);

		return fn(ctx, req, rsp)
	}
}
