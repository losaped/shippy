package postgres

import (
	pb "bitbucket.org/losaped/shippy/user-service/proto/user"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	"github.com/satori/go.uuid"
)

type User struct {
	ID       string `gorm:"primary_key"`
	Name     string
	Company  string
	Email    string
	Password string
}

func (u *User) FromProto(pbuser *pb.User) {
	u.ID = pbuser.Id
	u.Name = pbuser.Name
	u.Email = pbuser.Email
	u.Company = pbuser.Company
	u.Password = pbuser.Password
}

func (u *User) ToProto() *pb.User {
	return &pb.User{
		Id:       u.ID,
		Name:     u.Name,
		Company:  u.Company,
		Email:    u.Email,
		Password: u.Password,
	}
}

func (u *User) BeforeCreate(scope *gorm.Scope) error {
	return scope.SetColumn("ID", uuid.NewV4().String())
}

func GetRepository(constring string) (*UserRepository, error) {
	con, err := gorm.Open("postgres", constring)
	if err != nil {
		return nil, err
	}

	con.AutoMigrate(&User{})
	return &UserRepository{con}, nil
}

type UserRepository struct {
	db *gorm.DB
}

func (repo *UserRepository) GetAll() ([]*pb.User, error) {
	var users []User
	if err := repo.db.Find(&users).Error; err != nil {
		return nil, err
	}

	pbusers := make([]*pb.User, 0, len(users))
	for _, u := range users {
		pbusers = append(pbusers, u.ToProto())
	}

	return pbusers, nil
}

func (repo *UserRepository) Get(id string) (*pb.User, error) {
	var user *User
	user.ID = id
	if err := repo.db.First(&user).Error; err != nil {
		return nil, err
	}
	return user.ToProto(), nil
}

func (repo *UserRepository) GetByEmail(email string) (*pb.User, error) {
	u := &User{}
	if err := repo.db.Where(&User{Email: email}).First(u).Error; err != nil {
		return nil, err
	}
	return u.ToProto(), nil
}

func (repo *UserRepository) Create(user *pb.User) error {
	u := &User{}
	u.FromProto(user)
	if err := repo.db.Create(u).Error; err != nil {
		return err
	}

	return nil
}
