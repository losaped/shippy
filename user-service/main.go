package main

import (
	"fmt"
	"log"
	"os"

	"bitbucket.org/losaped/shippy/user-service/postgres"
	pb "bitbucket.org/losaped/shippy/user-service/proto/user"
	micro "github.com/micro/go-micro"
)

type Repository interface {
	GetAll() ([]*pb.User, error)
	Get(id string) (*pb.User, error)
	Create(user *pb.User) error
	GetByEmail(email string) (*pb.User, error)
}

func main() {
	host := os.Getenv("DB_HOST")
	port := os.Getenv("DB_PORT")
	dbname := os.Getenv("DB_NAME")
	user := os.Getenv("DB_USER")
	pass := os.Getenv("DB_PASSWORD")

	constr := fmt.Sprintf("host=%s port=%s user=%s password=%s dbname=%s sslmode=disable", host, port, user, pass, dbname)
	repo, err := postgres.GetRepository(constr)
	if err != nil {
		log.Fatal(err)
	}

	userSvc := micro.NewService(micro.Name("user"))
	userSvc.Init()

	handler := &Handler{
		repo: repo,
		tokenService: &TokenService{
			repo: repo,
		},
	}

	pb.RegisterUserServiceHandler(userSvc.Server(), handler)

	if err := userSvc.Run(); err != nil {
		log.Fatalf("failed to serve: %v", err)
	}

}
