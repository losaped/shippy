package main

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"os"

	pb "bitbucket.org/losaped/shippy/consignment-service/proto/consignment"
	micro "github.com/micro/go-micro"
	"github.com/micro/go-micro/metadata"
)

const (
	address         = "localhost:50051"
	defaultFilename = "./consignment.json"
)

func parseFile(file string) (*pb.Consignment, error) {
	var consignment *pb.Consignment
	data, err := ioutil.ReadFile(file)
	if err != nil {
		return nil, err
	}

	json.Unmarshal(data, &consignment)
	return consignment, err
}

func main() {
	var token string
	file := defaultFilename
	if len(os.Args) < 3 {
		log.Fatal(errors.New("not enough arguments, expecing file and token"))
	}

	fmt.Printf("%v", os.Args)
	file = os.Args[1]
	token = os.Args[2]

	fmt.Println("trying parse file:", file)
	consignment, err := parseFile(file)
	if err != nil {
		log.Fatalf("Could not parse file: %v", err)
	}

	fmt.Println("access token:", token)
	svc := micro.NewService(micro.Name("consignment.client"))
	svc.Init()

	client := pb.NewShippingService("consignment", svc.Client())

	ctx := metadata.NewContext(context.Background(), map[string]string{
		"token": token,
	})

	r, err := client.CreateConsignment(ctx, consignment)
	if err != nil {
		log.Fatalf("Could not cleate consignment: %v", err)
	}

	log.Printf("Created: %t", r.Created)

	getAll, err := client.GetConsignments(context.Background(), &pb.GetRequest{})
	if err != nil {
		log.Fatalf("Could not list consignments: %v", err)
	}

	for _, v := range getAll.Consignments {
		log.Println(v)
	}
}
