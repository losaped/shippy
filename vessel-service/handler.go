package main

import (
	"context"

	pb "bitbucket.org/losaped/shippy/vessel-service/proto/vessel"
)

type handler struct {
	repo Repository
}

func (h *handler) FindAvailable(ctx context.Context, spec *pb.Specification, resp *pb.Response) (err error) {
	v, err := h.repo.FindAvailable(spec)
	resp.Vessel = v
	return err
}

func (h *handler) Create(ctx context.Context, req *pb.Vessel, res *pb.Response) error {
	if err := h.repo.Create(req); err != nil {
		return err
	}

	res.Vessel = req
	res.Created = true
	return nil
}
