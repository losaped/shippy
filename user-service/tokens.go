package main

import (
	"fmt"
	"time"

	pb "bitbucket.org/losaped/shippy/user-service/proto/user"
	jwt "github.com/dgrijalva/jwt-go"
)

var secret = []byte("MySupperSecretSalt")

type CustomClaims struct {
	User *pb.User
	jwt.StandardClaims
}

type Authable interface {
	Encode(*pb.User) (string, error)
	Decode(token string) (*CustomClaims, error)
}

type TokenService struct {
	repo Repository
}

func (srv *TokenService) Decode(tokenStr string) (*CustomClaims, error) {
	token, err := jwt.ParseWithClaims(tokenStr, &CustomClaims{}, func(token *jwt.Token) (interface{}, error) {
		return secret, nil
	})

	fmt.Println("TRY PARSE TOKEN", tokenStr)
	if claims, ok := token.Claims.(*CustomClaims); ok && token.Valid {
		return claims, nil
	}

	fmt.Println("TRY PARSE TOKEN", tokenStr)
	return nil, err
}

func (srv *TokenService) Encode(user *pb.User) (string, error) {
	expireToken := time.Now().Add(time.Hour * 72).Unix()

	claims := CustomClaims{
		user,
		jwt.StandardClaims{
			ExpiresAt: expireToken,
			Issuer:    "go.micro.srv.user",
		},
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	return token.SignedString(secret)
}
